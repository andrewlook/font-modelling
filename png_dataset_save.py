import os
import numpy as np

from PIL import Image

from char_imgs import gen_char_imgs


def png_samples(base_path, categories=[], post_script_name_suffix=None, limit=None, verbose=False):
  if not os.path.isdir(base_path):
      os.makedirs(base_path)
  for img, char, font, font_family, family_dir in gen_char_imgs(categories=categories, post_script_name_suffix=post_script_name_suffix, limit=limit):
      fname = f"{char}_{font_family.category}_{font_family.name}_{font.post_script_name}_{font.style}_{font.weight}.png"
      out_path = os.path.join(base_path, fname)
      img.save(out_path)
      if verbose:
        print(f'saved {out_path}')


def list_pngs(base_path, suffix=None, limit=None):
  for i, fname in enumerate(os.listdir(base_path)):
    if suffix:
      if suffix not in fname:
        continue
    fpath = os.path.join(base_path, fname)
    char = fname.split('_')[0]
    img = Image.open(fpath)
    yield np.array(img.convert(mode='L')), char
    if limit and i > limit:
      return


if __name__ == '__main__':
    import sys
    argv = sys.argv

    out_dir = argv[1]
    num_families = int(argv[2])
    kwargs = {
        'limit': num_families,
        'verbose': True,
    }
    if len(argv) > 3:
        kwargs['categories'] = argv[3]
    if len(argv) > 4:
        kwargs['post_script_name_suffix'] = argv[4]

    png_samples(out_dir, **kwargs)

