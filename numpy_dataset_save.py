import os
import os.path as osp

import numpy as np

from char_imgs import gen_char_imgs


def numpy_samples(categories=[], post_script_name_suffix=None, limit=None):
  all_arrs = []
  all_chars = []
  for img, char, font, font_family, family_dir in gen_char_imgs(categories=categories, post_script_name_suffix=post_script_name_suffix, limit=limit):
    arr = np.array(img.convert(mode='L'))
    all_arrs.append(arr)
    all_chars.append(np.uint8(ord(char)))
  # TODO(shapes): refactor out hardcoded 28's here
  images = np.stack(all_arrs).reshape((len(all_arrs), 28, 28, 1))
  labels = np.stack(all_chars)
  return images, labels


if __name__ == '__main__':
  import sys
  argv = sys.argv
 
  out_path = argv[1]
  num_families = int(argv[2])
  kwargs = {
  	'limit': num_families,
  }
  if len(argv) > 3:
  	kwargs['categories'] = argv[3]
  if len(argv) > 4:
  	kwargs['post_script_name_suffix'] = argv[4]
  
  images, labels = numpy_samples(**kwargs)

  out_images = out_path + '.images'
  out_labels = out_path + '.labels'

  # stub until I have code to save these tensors
  print(out_images, images.shape)
  print(out_labels, labels.shape)
