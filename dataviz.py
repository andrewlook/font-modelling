import numpy as np
import matplotlib.pyplot as plt

from numpy_dataset_save import numpy_samples


def numpy_arrays_to_canvas(input_arr, dim=20):
    canvas = np.zeros((28*dim, 28*dim))
    for i in range(dim):
        for j in range(dim):
            image_idx = (i * dim) + j
            if image_idx < len(input_arr):
                canvas[i*28:(i+1)*28, j*28:(j+1)*28] = input_arr[image_idx].reshape(28, 28)
    return canvas


def plot_canvas(input_arr, dim=20):
    plt.figure(figsize=(8, 10))
    # Xi, Yi = np.meshgrid(x_values, y_values)
    canvas = numpy_arrays_to_canvas(input_arr, dim=dim)
    plt.imshow(canvas, origin="upper", cmap="gray")
    plt.tight_layout()


def plot_numpy_samples(**kwargs):
    """

    ex. plot_numpy_samples(categories='SANS_SERIF', post_script_name_suffix='Italic', limit=2)

    :param kwargs:
    :return:
    """
    images, _ = numpy_samples(**kwargs)
    plot_canvas(images)
