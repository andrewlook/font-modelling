#!/usr/bin/env bash

set -euo

. ./scripts/common.bash

[[ -d ${DATA_NUMPY}/ ]] && (echo "clearing ${DATA_NUMPY}/" && rm -rf "${DATA_NUMPY}"/ )

python font_samples_numpy.py "${DATA_NUMPY}/SANS_SERIF" 2 SANS_SERIF

python font_samples_numpy.py "${DATA_NUMPY}/ITALIC_SANS_SERIF" 2 SANS_SERIF Italic
