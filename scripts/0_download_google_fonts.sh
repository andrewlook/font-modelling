#!/usr/bin/env bash

set -euo

. ./scripts/common.bash

mkdir -p ./data

cd ./data

[[ -f master.zip ]] || (echo "dowloading fonts" && wget -q https://github.com/google/fonts/archive/master.zip)
ls -lth master.zip

[[ -d fonts-master ]] || (echo 'Unzipping' && unzip -q master.zip)
ls -l fonts-master/ofl/ | head
