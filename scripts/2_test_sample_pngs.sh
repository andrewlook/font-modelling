#!/usr/bin/env bash

set -euo

. ./scripts/common.bash

[[ -d ${DATA_PNG} ]] && (echo 'clearing ./samples' && rm -rf "${DATA_PNG}" )

mkdir -p "${DATA_PNG}"

python font_samples_png.py "${DATA_PNG}/SANS_SERIF" 2 SANS_SERIF

python font_samples_png.py "${DATA_PNG}/ITALIC_SANS_SERIF" 2 SANS_SERIF Italic
