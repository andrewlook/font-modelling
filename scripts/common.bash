#!/usr/bin/env bash

export GOOGLE_FONTS="./data/fonts-master"

export DATA_NUMPY="./data/numpy"
export DATA_PNG="./data/png"
