#!/usr/bin/env bash

set -euo

. ./scripts/common.bash

mkdir -p google_fontbakery

pushd google_fontbakery

touch __init__.py

[[ -f fonts_public.proto ]] || (echo "dowloading fonts_public.proto" && wget -q https://raw.githubusercontent.com/googlefonts/fontbakery/master/Lib/fontbakery/fonts_public.proto )

[[ -f fonts_public_pb2.py ]] || (echo "dowloading fonts_public_pb2.py" && wget -q https://raw.githubusercontent.com/googlefonts/fontbakery/master/Lib/fontbakery/fonts_public_pb2.py )

popd

echo GOOGLE_FONTS = "$GOOGLE_FONTS"

python font_family.py "${GOOGLE_FONTS}/ofl/monda/METADATA.pb"
