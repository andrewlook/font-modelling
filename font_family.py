""" Finds and parses font family metadata

Example of a parsed font family (<class 'fonts_public_pb2.FamilyProto'>)

    name: "Monda"
    designer: "Vernon Adams"
    license: "OFL"
    category: "SANS_SERIF"
    date_added: "2012-11-30"
    fonts {
      name: "Monda"
      style: "normal"
      weight: 400
      filename: "Monda-Regular.ttf"
      post_script_name: "Monda-Regular"
      full_name: "Monda Regular"
      copyright: "Copyright (c) 2012, vernon adams ..."
    }
    fonts {
      name: "Monda"
      style: "normal"
      weight: 700
      filename: "Monda-Bold.ttf"
      post_script_name: "Monda-Bold"
      full_name: "Monda Bold"
      copyright: "Copyright (c) 2012, vernon adams ..."
    }
    subsets: "menu"
    subsets: "latin"
    subsets: "latin-ext"

"""
import os
import os.path as osp

from google.protobuf import text_format

from google_fontbakery.fonts_public_pb2 import FamilyProto


def list_font_families(categories=[], limit=None):
  i = 0
  for license_type in ['ofl', 'ufl', 'apache']:
    base_path = osp.join('./data/fonts-master', license_type)
    for typeface in os.listdir(base_path):
      family_dir = os.path.join(base_path, typeface)
      metadata_fname = osp.join(family_dir, 'METADATA.pb')
      if not os.path.isfile(metadata_fname):
        print(f'{metadata_fname} does not exist')
        continue
      font_family = get_FamilyProto_Message(metadata_fname)
      if categories and font_family.category not in categories:
        print(f'{font_family.category} not in {categories}')
        continue
      yield font_family, family_dir

      i += 1
      if limit and i > limit:
        return


def list_fonts(font_families, post_script_name_suffix=None):
  for font_family, family_dir in font_families:
    for font in font_family.fonts:
      if post_script_name_suffix and not font.post_script_name.endswith(post_script_name_suffix):
        print(f'not {font.post_script_name}.endswith({post_script_name_suffix})')
        continue
      yield font, font_family, family_dir


def get_FamilyProto_Message(path):
  message = FamilyProto()
  text_data = open(path, "rb").read()
  text_format.Merge(text_data, message)
  return message


if __name__ == '__main__':
  import sys
  out = get_FamilyProto_Message(sys.argv[1])
  print(type(out))
  print(out)
