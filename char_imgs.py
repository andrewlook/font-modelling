import os
import os.path as osp
from string import ascii_lowercase

from PIL import ImageFont, ImageDraw, Image

from font_family import list_font_families, list_fonts


# TODO(rename): render_chars might be a better name...
def create_samples_for_font(ttf_abs_path,
                            charset=ascii_lowercase,
                            font_size=20,
                            img_size=28,
                            start_x=4,
                            start_y=4):
  try:
    font = ImageFont.truetype(ttf_abs_path, font_size)
    for char in charset:
      img = Image.new("RGB", (img_size, img_size), "black")
      draw = ImageDraw.Draw(img)
      draw.text((start_x, start_y), char, font=font, fill='white')
      yield char, img   
  except Exception as e:
    print(f'skipping "{ttf_abs_path}": {e}')


def gen_char_imgs(categories=[], post_script_name_suffix=None, limit=None):
  font_families = list_font_families(limit=limit, categories=categories)
  for font, font_family, family_dir in list_fonts(font_families, post_script_name_suffix=post_script_name_suffix):
    for char, img in create_samples_for_font(osp.join(family_dir, font.filename), ascii_lowercase):
      yield img, char, font, font_family, family_dir

