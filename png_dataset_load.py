import numpy as np

from tensorflow.python.framework import dtypes

from dataset import _DataSet, _Datasets
from png_dataset_save import list_pngs


def read_png_datasets(train_dir,
                      fake_data=False,
                      one_hot=False,
                      dtype=dtypes.float32,
                      reshape=True,
                      validation_size=100,
                      seed=None):
    if fake_data:
        def fake():
            return _DataSet([], [],
                            fake_data=True,
                            one_hot=one_hot,
                            dtype=dtype,
                            seed=seed)

        train = fake()
        validation = fake()
        test = fake()
        return _Datasets(train=train, validation=validation, test=test)

    all_arrs = []
    all_chars = []
    for arr, char in list_pngs(train_dir):
        all_arrs.append(arr)
        all_chars.append(np.uint8(ord(char)))

    train_images = np.stack(all_arrs).reshape((len(all_arrs), 28, 28, 1))
    train_labels = np.stack(all_chars)
    print(train_images.shape, train_labels.shape)

    if not 0 <= validation_size <= len(train_images):
        raise ValueError(
            'Validation size should be between 0 and {}. Received: {}.'.format(
                len(train_images), validation_size))

    validation_images = train_images[:validation_size]
    validation_labels = train_labels[:validation_size]
    train_images = train_images[validation_size:]
    train_labels = train_labels[validation_size:]

    options = dict(dtype=dtype, reshape=reshape, seed=seed)

    train = _DataSet(train_images, train_labels, **options)
    validation = _DataSet(validation_images, validation_labels, **options)
    # test = _DataSet(test_images, test_labels, **options)

    return _Datasets(train=train, validation=validation, test=None)
