DIRNAME := $(shell pwd)


# For now, run this manually when you add a notebook. We can have CI do this
# automatically if it's too much friction to do this after adding a notebook
# to the docs folder.
nbconvert-docs: clean-nbconvert-docs
	for f in `find -d ./ -iname "*.ipynb"`; do echo "converting $$f"; jupyter nbconvert --to html $$f; done;

clean-nbconvert-docs:
	find -d ./ -iname ".ipynb_checkpoints"
	find -d ./ -iname ".ipynb_checkpoints" | xargs rm -rf
	find -d ./ -iname "*.html"
	find -d ./ -iname "*.html" | xargs rm -rf
